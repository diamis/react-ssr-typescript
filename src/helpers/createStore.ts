import { createStore, applyMiddleware, compose } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './rootReducer'
import rootSaga from './rootSaga'

let composeEnhancers = compose
const middleware = []

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any
    }
}

if (process.env.NODE_ENV !== 'production') {
    const { createLogger } = require('redux-logger')
    middleware.push(createLogger({ collapsed: true }))

    /* Redux DevTools */
    if (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
}

/* SAGA MIDDLEWARE */
const sagaMiddleware = createSagaMiddleware()
middleware.push(sagaMiddleware)

const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)))

sagaMiddleware.run(rootSaga)

export { store }
