import * as React from 'react'
import loadable from '@loadable/component'

import { Loading } from 'components/Loading'

const AsyncHome = loadable(() => import('pages/Home'), { fallback: <Loading /> })
const AsyncInfo = loadable(() => import('pages/Info'), { fallback: <Loading /> })

const routers: {
    key: string | number | undefined
    name: string | number | undefined
    menu?: boolean
    path?: string | undefined
    exact?: boolean | undefined
    component?: React.FC<{}> | any
}[] = [
    {
        key: 'home',
        menu: true,
        name: 'Главная',
        path: '/',
        exact: true,
        component: AsyncHome,
    },
    {
        key: 'info',
        menu: true,
        name: 'О компании',
        path: '/info',
        exact: true,
        component: AsyncInfo,
    },
]

export { routers }
