declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any
    }
}

// IMAGE
declare module '*.gif' {
    const src: string
    export default src
}

declare module '*.jpg' {
    const src: string
    export default src
}

declare module '*.jpeg' {
    const src: string
    export default src
}

declare module '*.png' {
    const src: string
    export default src
}

declare module '*.svg' {
    import * as React from 'react'
    export const ReactComponent: React.FC<React.SVGProps<SVGSVGElement>>

    const src: string
    export default src
}

// STYLE
declare module '*.module.css' {
    const css: { [key: string]: string }
    export default css
}

declare module '*.css' {
    export default any
}

declare module '*.module.scss' {
    const css: { [key: string]: string }
    export default css
}

declare module '*.scss' {
    export default any
}

declare module '*.module.less' {
    const css: { [key: string]: string }
    export default css
}

declare module '*.less' {
    export default any
}

// CONST
declare const IS_SERVER: boolean
declare const IS_CLIENT: boolean
