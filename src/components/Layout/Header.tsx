import * as React from 'react'
import cn from 'classnames'

import { Logo } from 'components/Logo'
import { Menu } from 'components/Layout'

import style from './Header.module.scss'

type Props = {}

class Header extends React.PureComponent<Props, {}> {
    render() {
        return (
            <div className={cn('lineBorder', style.contener)}>
                <div className={cn('contener', style.items)}>
                    <Logo />
                    <Menu />
                </div>
            </div>
        )
    }
}

export { Header }
