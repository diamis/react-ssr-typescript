import * as React from 'react'

import { Header } from 'components/Layout'
import { Footer } from 'components/Layout'

import 'asset/styles/index.scss'
import './Layout.scss'

type Props = {
    children?: React.ReactNode
}

class Layout extends React.PureComponent<Props, {}> {
    render() {
        return (
            <>
                <Header />
                <main>{this.props.children}</main>
                <Footer />
            </>
        )
    }
}

export { Layout }
