import * as React from 'react'
import { NavLink } from 'react-router-dom'
import cn from 'classnames'

import { routers } from 'helpers'
import style from './Menu.module.scss'

type Props = {
    className?: string
}

const items = routers.filter((router) => router.hasOwnProperty('menu') && router.menu === true)

class Menu extends React.PureComponent<Props, {}> {
    render() {
        const { className } = this.props
        return (
            <div className={cn(className, style.menu)}>
                {items &&
                    items.map((item) => (
                        <NavLink key={item.key} to={`${item.path}`} className={style.menuItem}>
                            {item.name}
                        </NavLink>
                    ))}
            </div>
        )
    }
}

export { Menu }
