import * as React from 'react'
import cn from 'classnames'

import style from './Logo.module.scss'

type Props = {
    className?: string
}

class Logo extends React.PureComponent<Props, {}> {
    render() {
        const { className } = this.props

        return (
            <div className={cn(className, style.logo)}>
                <span className={style.logoLine}>React</span>
                <span className={style.logoLine}>TypeScript</span>
            </div>
        )
    }
}

export { Logo }
