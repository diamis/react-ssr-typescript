import * as React from 'react'

const Loading = () => {
    return (
        <div className="loading">
            <div className="loadingSpinner">
                <div className="loadingSpinnerRect loadingSpinnerRect1"></div>
                <div className="loadingSpinnerRect loadingSpinnerRect2"></div>
                <div className="loadingSpinnerRect loadingSpinnerRect3"></div>
                <div className="loadingSpinnerRect loadingSpinnerRect4"></div>
                <div className="loadingSpinnerRect loadingSpinnerRect5"></div>
            </div>
        </div>
    )
}

export { Loading }
