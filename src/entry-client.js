import React from 'react'
import { hydrate } from 'react-dom'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import { loadableReady } from '@loadable/component'

import { history, store } from 'helpers'
import Root from 'Root'

if (typeof window !== 'undefined') {
    loadableReady(() => {
        hydrate(
            <Provider store={store}>
                <Router history={history}>
                    <Root />
                </Router>
            </Provider>,
            document.getElementById('root')
        )
    })

    if (process.env.NODE_ENV === 'development') {
        if (module.hot) {
            module.hot.accept()
        }
    }
}
