import React, { PureComponent } from 'react'
import { Switch, Route } from 'react-router-dom'

import { routers } from 'helpers'

class Root extends PureComponent {
    render() {
        return (
            <Switch>
                {routers.map((router) => {
                    return <Route {...router} component={router.component} />
                })}
            </Switch>
        )
    }
}

export default Root
