import path from 'path'
import chalk from 'chalk'
import express from 'express'
import manifestHelpers from 'express-manifest-helpers'
import cors from 'cors'
import bodyParser from 'body-parser'
import favicon from 'serve-favicon'

import Root from 'Root'
import paths from '../config/paths'
import { serverRenderer } from 'middleware'

require('dotenv').config()

const { PORT = 3000 } = process.env
const manifestPath = path.join(paths.buildClient, paths.publicPath, 'manifest.json')
const app = express()

/**
 * Для продакшена рекомендуется выполнять обработку статических файлов
 * испльзую Nginx или Apatch.
 */
// if (process.env.NODE_ENV === 'development') {
app.use(favicon(path.join(paths.buildClient, 'favicon.ico')))
app.use(paths.publicPath, express.static(path.join(paths.buildClient, paths.publicPath)))
// }

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use(manifestHelpers({ manifestPath }))

app.use(serverRenderer)

app.listen(PORT, () => {
    const message = `Server: http://localhost:${PORT}`
    console.log(`[${new Date().toISOString()}]`, chalk.green(message))
})
