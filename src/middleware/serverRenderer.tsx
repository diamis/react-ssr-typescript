import fs from 'fs'
import { join } from 'path'
import * as React from 'react'
import * as express from 'express'
import { Helmet } from 'react-helmet'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom'
import { renderToString } from 'react-dom/server'
import { ChunkExtractor } from '@loadable/server'

import Root from 'Root'
import paths from '../../config/paths'
import { store } from 'helpers'

const serverRenderer: any = (req: express.Request, res: express.Response) => {
    const url = req.originalUrl || req.url
    const file = join(paths.buildClient, 'index.html')
    const fileStats = join(paths.buildClient, paths.publicPath, 'loadable-stats.json')

    if (process.env.NODE_ENV === 'development') {
        console.log(`[${new Date().toISOString()}] Render url: ${url}`)
    }

    fs.readFile(file, 'utf-8', (err: any, html: string) => {
        if (err) {
            console.log('Something went wrong:', err)
            return res.status(500).send('Oops, better luck next time!')
        }

        const routerContext: any = {}
        const extractor = new ChunkExtractor({
            statsFile: fileStats,
            entrypoints: ['bundle'],
        })

        const jsx = extractor.collectChunks(
            <Provider store={store}>
                <StaticRouter location={url} context={routerContext}>
                    <Root />
                </StaticRouter>
            </Provider>
        )

        if (routerContext.url) {
            res.redirect(routerContext.url)
            return
        }

        const content = renderToString(jsx)
        const helmet: any = Helmet.renderStatic()

        const styleTags = extractor.getStyleTags()
        const scriptTags = extractor.getScriptTags()

        html = html.replace('</head>', `${helmet.title.toString()}</head>`)
        html = html.replace('</head>', `${helmet.meta.toString()}</head>`)
        html = html.replace('</head>', `${styleTags}</head>`)
        html = html.replace('<div id="root"></div>', `<div id="root">${content}</div>`)
        html = html.replace('</body>', `${scriptTags}</body>`)

        return res.send(html)
    })
}

export { serverRenderer }
