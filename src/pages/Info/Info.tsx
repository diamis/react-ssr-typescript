import * as React from 'react'
import { Helmet } from 'react-helmet'

import { Layout } from 'components/Layout'

type Props = {}

class Info extends React.PureComponent<Props, {}> {
    render() {
        return (
            <>
                <Helmet>
                    <title>React Info</title>
                    <meta name="description" content="React description" />
                </Helmet>
                <Layout>Info</Layout>
            </>
        )
    }
}

export { Info }
