import * as React from 'react'
import { Helmet } from 'react-helmet'

import { Layout } from 'components/Layout'

import reactImage from 'asset/img/react.png'

type Props = {}

class Home extends React.PureComponent<Props, {}> {
    render() {
        return (
            <>
                <Helmet>
                    <title>React HOME</title>
                    <meta name="description" content="React description" />
                </Helmet>
                <Layout>
                    <h1>SSR React TypeScript</h1>
                    <p>layout component (scss)</p>
                    <img src={reactImage} />
                </Layout>
            </>
        )
    }
}

export { Home }
