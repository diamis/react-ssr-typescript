require('@babel/register')({
    presets: [
        [
            '@babel/preset-env',
            {
                modules: false,
                targets: {
                    node: 'current',
                },
            },
        ],
        '@babel/preset-typescript',
    ],
})

module.exports = require('./postcss.config.ts').default
