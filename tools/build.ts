import rimraf from 'rimraf'
import webpack from 'webpack'
import chalk from 'chalk'

import paths from '../config/paths'
import webpackConfig from '../config/webpack.config'
import { logMessage, compilerPromise } from './utils'

// Чистим директорию сборки
rimraf.sync(paths.buildClient)
rimraf.sync(paths.buildServer)

const build = async () => {
    const [clientConfig, serverConfig] = webpackConfig

    const multiCompiler = webpack([clientConfig, serverConfig])
    const clientCompiler = multiCompiler.compilers.find((conf) => conf.name === 'client')
    const serverCompiler = multiCompiler.compilers.find((conf) => conf.name === 'server')

    const clientPromise = compilerPromise('client', clientCompiler)
    const serverPromise = compilerPromise('server', serverCompiler)

    serverCompiler.watch({}, (error: any, stats: any) => {
        if (!error && !stats.hasErrors()) {
            console.log(stats.toString(serverConfig.stats))
            return
        }
        console.error(chalk.red(stats.compilation.errors))
    })

    clientCompiler.watch({}, (error: any, stats: any) => {
        if (!error && !stats.hasErrors()) {
            console.log(stats.toString(clientConfig.stats))
            return
        }
        console.error(chalk.red(stats.compilation.errors))
    })

    try {
        await serverPromise
        await clientPromise
        logMessage('Done!', 'info')
    } catch (error) {
        logMessage(error, 'error')
    }
}

build()
