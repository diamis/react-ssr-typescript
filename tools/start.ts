import rimraf from 'rimraf'
import webpack from 'webpack'
import express from 'express'
import nodemon from 'nodemon'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'

import paths from '../config/paths'
import webpackConfig from '../config/webpack.config'
import { logMessage, compilerPromise } from './utils'

// Чистим директорию сборки
rimraf.sync(paths.buildClient)
rimraf.sync(paths.buildServer)

const HOST = process.env.HOST || 'http://localhost'
const PORT = process.env.PORT || process.env.PORT ? Number(process.env.PORT) + 1 : 3001

const app = express()

const start = async () => {
    const [clientConfig, serverConfig] = webpackConfig
    const publicPath = clientConfig.output.publicPath
    const devPublicPath = [`${HOST}:${PORT}`, publicPath].join('/').replace(/([^:+])\/+/g, '$1/')

    clientConfig.entry.bundle = [
        `webpack-hot-middleware/client?path=${HOST}:${PORT}/__webpack_hmr`,
        ...clientConfig.entry.bundle,
    ]
    clientConfig.output.hotUpdateMainFilename = 'updates/[hash].hot-update.json'
    clientConfig.output.hotUpdateChunkFilename = 'updates/[hash].hot-update.chunk.js'

    clientConfig.output.publicPath = devPublicPath
    serverConfig.output.publicPath = devPublicPath

    const multiCompiler = webpack([clientConfig, serverConfig])
    const clientCompiler: any = multiCompiler.compilers.find((c) => c.name === 'client')
    const serverCompiler: any = multiCompiler.compilers.find((c) => c.name === 'server')

    const clientPromise = compilerPromise('client', clientCompiler)
    const serverPromise = compilerPromise('server', serverCompiler)

    const watchOptions = {
        ignored: /node_modules/,
        stats: clientConfig.stats,
    }

    app.use((_req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*')
        return next()
    })

    app.use(
        webpackDevMiddleware(clientCompiler, {
            stats: clientConfig.stats,
            publicPath: clientConfig.output.publicPath,
            watchOptions,
        })
    )
    app.use(webpackHotMiddleware(clientCompiler))

    app.use('/static', express.static(paths.buildClient))

    app.listen(PORT)

    serverCompiler.watch(watchOptions, (error, stats) => {
        if (!error && !stats.hasErrors()) {
            console.log(stats.toString(serverConfig.stats))
            return
        }

        if (error) {
            logMessage(error, 'error')
        }

        if (stats.hasErrors()) {
            const info = stats.toJson()
            const errors = info.errors[0].split('\n')
            logMessage(errors[0], 'error')
            logMessage(errors[1], 'error')
            logMessage(errors[2], 'error')
        }
    })

    try {
        await serverPromise
        await clientPromise
    } catch (error) {
        logMessage(error, 'error')
    }

    const script = nodemon({
        script: `${paths.buildServer}/${serverConfig.output.filename}`,
        ignore: ['src', 'rools', 'config', 'build/client', './*.*'],
        delay: 200,
    })

    script.on('restart', () => {
        logMessage('Server side app restarted', 'warning')
    })

    script.on('quit', () => {
        console.log('Process ended')
        process.exit()
    })

    script.on('error', () => {
        logMessage('Server side app restarted', 'error')
    })
}

start()
