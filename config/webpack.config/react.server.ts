import webpack from 'webpack'
import nodeExternals from 'webpack-node-externals'

import createConfig from './react.root'
import paths from '../paths'

const config = createConfig({ target: 'server' })

export default {
    ...config.webpack,

    target: 'node',

    entry: {
        server: [
            require.resolve('core-js/stable'),
            require.resolve('regenerator-runtime/runtime'),
            paths.entryServer,
        ],
    },

    output: {
        path: paths.buildServer,
        filename: 'express.js',
        publicPath: paths.publicPath,
    },

    plugins: [
        ...config.webpack.plugins,

        config.isDev && new webpack.HotModuleReplacementPlugin(),
    ].filter(Boolean),

    externals: [nodeExternals({ whitelist: /\.css$/ })],

    stats: {
        assets: false,
        cached: false,
        cachedAssets: false,
        chunks: false,
        chunkModules: false,
        children: false,
        colors: true,
        hash: false,
        modules: false,
        performance: false,
        reasons: false,
        timings: true,
        version: false,
    },

    node: {
        __dirname: false,
    },
}
