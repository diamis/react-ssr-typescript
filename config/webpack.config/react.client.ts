import ManifestPlugin from 'webpack-manifest-plugin'
import TerserPlugin from 'terser-webpack-plugin'
import CopyPlugin from 'copy-webpack-plugin'
import webpack from 'webpack'
import { join } from 'path'

import createConfig from './react.root'
import paths from '../paths'

const config = createConfig({ target: 'client' })

export default {
    ...config.webpack,

    target: 'web',

    entry: {
        bundle: [
            require.resolve('core-js/stable'),
            require.resolve('regenerator-runtime/runtime'),
            paths.entryClient,
        ],
    },

    output: {
        ...config.webpack.output,

        path: join(paths.buildClient, paths.publicPath),
    },

    plugins: [
        ...config.webpack.plugins,

        new CopyPlugin([{ from: paths.public, to: paths.buildClient }]),

        new ManifestPlugin({ filename: 'manifest.json' }),

        config.isDev && new webpack.HotModuleReplacementPlugin(),
    ].filter(Boolean),

    optimization: {
        minimize: !config.isDev,
        minimizer: [new TerserPlugin()],
        namedModules: true,
        noEmitOnErrors: true,
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                },
            },
        },
    },

    stats: {
        cached: false,
        cachedAssets: false,
        chunks: false,
        chunkModules: false,
        children: false,
        colors: true,
        hash: false,
        modules: false,
        reasons: false,
        timings: true,
        version: false,
    },

    node: {
        module: 'empty',
        dgram: 'empty',
        dns: 'mock',
        fs: 'empty',
        http2: 'empty',
        net: 'empty',
        tls: 'empty',
        child_process: 'empty',
    },
}
