require('@babel/register')({
    presets: [
        [
            '@babel/preset-env',
            {
                modules: false,
                targets: {
                    node: 'current',
                },
            },
        ],
        '@babel/preset-typescript',
    ],
})
require('dotenv').config()

module.exports = require('./index.ts').default
