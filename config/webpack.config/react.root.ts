import { join } from 'path'
import webpack from 'webpack'
import LoadablePlugin from '@loadable/webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import WriteFileWebpackPlugin from 'write-file-webpack-plugin'

import paths from '../paths'
import babelConfig from '../../babel.config'

const { NODE_ENV } = process.env
const isDev: boolean = NODE_ENV !== 'production'

const regexCss = /\.css$/
const regexCssModule = /\.module\.css$/
const regexSass = /\.(scss|sass)$/
const regexSassModule = /\.module\.(scss|sass)$/
const regexLess = /\.less$/
const regexLessModule = /\.module\.less$/

/**
 * Выносим генератор Loader для стилей для избежания дублирования в коде.
 *
 * @param isClient указываем является сборка "клиенской" или "серверной"
 * @param params передает настроки "options" для css-loader
 * @param param2 препроцессор
 */
const getStyleLoader: any = (
    isClient: boolean,
    params: Object = {},
    { loader = null, options = {} }: { loader?: string; options?: any } = {}
) => {
    let resLoader = []

    if (isDev && isClient) resLoader.push({ loader: require.resolve('css-hot-loader') })

    resLoader.push({ loader: MiniCssExtractPlugin.loader })
    resLoader.push({ loader: require.resolve('css-loader'), options: params })

    if (isClient) resLoader.push({ loader: require.resolve('postcss-loader') })

    if (loader) resLoader.push({ loader: require.resolve(loader), options })

    return resLoader
}

export default ({ target }) => {
    const isServer: boolean = target === 'server'
    const isClient: boolean = target === 'client'

    const filename: string = isDev ? '[name].js' : '[name].[hash:8].js'
    const chunkFilename: string = isDev ? '[name].js' : '[name].[hash:8].chunk.js'

    return {
        isDev,
        isServer,
        isClient,

        webpack: {
            name: target,
            mode: isDev ? 'development' : 'production',

            output: {
                filename,
                chunkFilename,
                publicPath: paths.publicPath,
            },

            resolve: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                modules: paths.resolveModules,
            },

            module: {
                rules: [
                    {
                        oneOf: [
                            {
                                test: /\.(js|jsx|ts|tsx)$/,
                                exclude: /node_modules/,
                                loader: require.resolve('babel-loader'),
                                options: {
                                    ...babelConfig,
                                    cacheDirectory: true,
                                    cacheCompression: !isDev,
                                    compact: !isDev,
                                },
                            },
                            {
                                test: regexCss,
                                exclude: regexCssModule,
                                use: getStyleLoader(isClient),
                            },
                            {
                                test: regexCssModule,
                                use: getStyleLoader(isClient, {
                                    modules: true,
                                    importLoaders: 1,
                                    localsConvention: 'camelCase',
                                }),
                            },
                            {
                                test: regexSass,
                                exclude: regexSassModule,
                                use: getStyleLoader(
                                    isClient,
                                    { importLoaders: 2 },
                                    { loader: 'sass-loader' }
                                ),
                            },
                            {
                                test: regexSassModule,
                                use: getStyleLoader(
                                    isClient,
                                    {
                                        modules: true,
                                        importLoaders: 2,
                                        localsConvention: 'camelCase',
                                    },
                                    { loader: 'sass-loader' }
                                ),
                            },
                            {
                                test: /\.(png|jpe?g|gif|svg)$/,
                                exclude: /\/fonts\//,
                                loader: require.resolve('url-loader'),
                                options: {
                                    limit: 2048,
                                    name: 'media/[name].[hash:5].[ext]',
                                    emitFile: isClient,
                                },
                            },
                            {
                                exclude: /\.(js|jsx|ts|tsx|css|scss|sass|less|mjs|html|ejs|json)$/,
                                use: [
                                    {
                                        loader: require.resolve('file-loader'),
                                        options: {
                                            name: 'assets/[name].[hash:5].[ext]',
                                            emitFile: isClient,
                                        },
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },

            plugins: [
                new LoadablePlugin(),

                isDev && new WriteFileWebpackPlugin(),

                new MiniCssExtractPlugin({
                    filename: isDev ? 'css/[name].css' : 'css/[name].[hash:4].css',
                    chunkFilename: isDev ? 'css/[name].chunk.css' : 'css/[hash:4].chunk.css',
                }),

                new webpack.DefinePlugin({
                    'process.env.NODE_ENV': JSON.stringify(!isDev ? 'production' : 'development'),
                    'typeof window': JSON.stringify(isServer ? 'undefined' : 'object'),
                    'IS_SERVER': JSON.stringify(isServer ? true : false),
                    'IS_CLIENT': JSON.stringify(isClient ? true : false),
                }),

                new webpack.NamedModulesPlugin(),
            ].filter(Boolean),
        },
    }
}
