import { resolve } from 'path'

const resolveApp = (path: string) => resolve(process.cwd(), path)
const paths: any = {
    src: resolveApp('src'),
    build: resolveApp('build'),
    public: resolveApp('public'),
    buildServer: resolveApp('build/server'),
    buildClient: resolveApp('build/client'),
    entryServer: resolveApp('src/entry-server.js'),
    entryClient: resolveApp('src/entry-client.js'),

    publicPath: '/static/',
}

paths.resolveModules = [paths.src, 'node_modules']

export default paths
