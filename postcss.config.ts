import paths from './config/paths'

export default {
    sourceMap: true,
    plugins: [
        require('postcss-import')({
            path: [paths.src, `${__dirname}/node_modules`],
        }),
        require('postcss-nested')(),
        require('postcss-flexbugs-fixes')(),
        require('autoprefixer'),
        require('postcss-assets')({
            basePath: './assets',
        }),
    ],
}
